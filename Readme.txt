This repo contains a number of folders for which some details are provided below:

-----------------------------------------------------------------------------------------------------------------------------------------
2014.03.06 - 3D Truck Sim in VB6.0
-----------------------------------------------------------------------------------------------------------------------------------------
    Contains the following folder/files:
    
        - 3DVbTruck.zip
          Archive containing the source files and Visual Basic 6.0 project of an application that displays the 3D model of a truck and implements some basic control algorithms.
          
-----------------------------------------------------------------------------------------------------------------------------------------
2014.12.14 - First Visual Basic 6.0 Dashboard app
-----------------------------------------------------------------------------------------------------------------------------------------
    Contains the following folder/files:
    
        - 2014.12.14 - Dashboard VB.zip
          Archive containing the source files and Visual Basic 6.0 project of an application that is the first tentative to create a 'display and control' app for the Ao-to One board. Basically, it is the starting point for what the B4A project of the Android app will become later on. This application was running on PC and the communication with the Ao-to One board was done using a USB2Serial converter.
          
-----------------------------------------------------------------------------------------------------------------------------------------
2015.02.01 - Sources for Android phone app and STM32F4 Discovery board used for first movie of Ao-to One + G
-----------------------------------------------------------------------------------------------------------------------------------------
    Contains the following folder/files:
    
        - 2015.02.01 - Dashboard.zip
          Archive containing the B4A (Basic for Android) project that will generate the Android application used to display information from and control the Ao-to One board. The project is the one that has been used on 1st of Feb 2015 to make the first movie with the suspension and phone control in action.
       
        - 2015.02.01 - Driver.zip
          Archive containing the source files and IAR project of the sw running on Ao-to One board, as of 1st of Feb 2015. The following files are empty and do not contain any code, due to the fact that their code was started from some sample code files released in source code by Freescale in it's 802.15.4 stacks (SynkroRF, RF4CE, Zigbee) codebases and the original header in these files state that no part of the code can be reproduced in any form without specific written permission from Freescale Semiconductor. For this reason, the IAR project will of course not compile. If approval from NXP to make these files available as open source will be obtained, their content will be added in the archive later on.
            - Source\Mcal\Base\FunctionLib.c
            - Source\Mcal\Base\FunctionLib.h
            - Source\Mcal\Gpt\Gpt.c
            - Source\Mcal\Gpt\Gpt.h
            - Source\Mcal\Gpt\Gpt_Config.h
            - Source\Mcal\Nvm\Nvm_Interface.h
            - Source\Mcal\Nvm\NV_Flash.c
            - Source\Mcal\Nvm\NV_Flash.h
            - Source\Mcal\Nvm\NV_FlashHAL.c
            - Source\Mcal\Nvm\NV_FlashHAL.h
            - Source\Mcal\Uart\Uart.c
            - Source\Mcal\Uart\Uart.h
            - Source\Os\Os.c
            - Source\Os\Os.h
            - Source\Os\Os_Cfg.h

-----------------------------------------------------------------------------------------------------------------------------------------
2015.11.02 - Latest version of sources for all targeted apps
-----------------------------------------------------------------------------------------------------------------------------------------
    Contains the following folder/files:
        - 01_SSB.zip
          Archive containing the source files and IAR project for a Second Stage Bootloader application. The binary generated from this project was supposed to be loaded during the production phase in every Ao-to One board before being delivered to users. After board was flashed with this binary, the Flash was supposed to be locked, in order not to be readable through the bootloader code that comes already written in the STM32F4 processors from ST. After the chip was booting, it was supposed to launch the SSB. With the help of a jumper on the board, the SSB would have taken the decision to launch the actual Ao-to One app from Flash or to wait an Ao-to One app to be received over UART. If jumper position was set to wait for an Uart image, the SSB was supposed to wait serial messages over UART, getting blocks of an Ao-to One app which was encrypted using AES and an unique key per board. This was supposed to be the mechanism of updating the Ao-to One sw in the field, after launching the product with initial sw. If new features were developped by Ao-to One team, a new binary image was supposed to be created and encrypted separately for every board already sold, with the key of that board. This would have made possible having different images sent to different users, each of them having new features enabled based on the fact that those feature were free or user should have paid for them. 
          
          The following files are empty and do not contain any code, due to the fact that their code was started from some sample code files released in source code by Freescale in it's 802.15.4 stacks (SynkroRF, RF4CE, Zigbee) codebases and the original header in these files state that no part of the code can be reproduced in any form without specific written permission from Freescale Semiconductor. For this reason, the IAR project will of course not compile. If approval from NXP to make these files available as open source will be obtained, their content will be added in the archive later on.
            - 01_SSB\Source\App\FunctionLib\FunctionLib.c
            - 01_SSB\Source\App\FunctionLib\FunctionLib.h
            - 01_SSB\Source\Mcal\Tmr\Timer.c
            - 01_SSB\Source\Mcal\Tmr\Timer.h
            - 01_SSB\Source\Mcal\Uart\Uart.c
            - 01_SSB\Source\Mcal\Uart\Uart.h
            
        - 02_VB6_SSB_Uploader.zip
        Archive containing the source files and Visual Basic 6.0 project of an application that was supposed to communicate over UART with the bootloader pre-flashed by ST in their STM32F407 device. The scope of this application was supposed to be the load of the SSB binary (see topic above) in the Flash of the STM32F407 device.
        
        - 03_Driver.zip
          Archive containing the source files and IAR project for Ao-to One application, as of 2nd of Nov 2015, just before movies for Kickstarter campaign were shot. The board that was used in the truck and so the target of this application was the STM32F4 Discovery board.
        
          The following files are empty and do not contain any code, due to the fact that their code was started from some sample code files released in source code by Freescale in it's 802.15.4 stacks (SynkroRF, RF4CE, Zigbee) codebases and the original header in these files state that no part of the code can be reproduced in any form without specific written permission from Freescale Semiconductor. For this reason, the IAR project will of course not compile. If approval from NXP to make these files available as open source will be obtained, their content will be added in the archive later on.
            - 03_Driver\Source\Mcal\Base\FunctionLib.c
            - 03_Driver\Source\Mcal\Base\FunctionLib.h
            - 03_Driver\Source\Mcal\Gpt\Gpt.c
            - 03_Driver\Source\Mcal\Gpt\Gpt.h
            - 03_Driver\Source\Mcal\Gpt\Gpt_Config.h
            - 03_Driver\Source\Mcal\Nvm\Nvm_Interface.h
            - 03_Driver\Source\Mcal\Nvm\NV_Flash.c
            - 03_Driver\Source\Mcal\Nvm\NV_Flash.h
            - 03_Driver\Source\Mcal\Nvm\NV_FlashHAL.c
            - 03_Driver\Source\Mcal\Nvm\NV_FlashHAL.h
            - 03_Driver\Source\Mcal\Uart\Uart.c
            - 03_Driver\Source\Mcal\Uart\Uart.h
            - 03_Driver\Source\Os\Os.c
            - 03_Driver\Source\Os\Os.h
            - 03_Driver\Source\Os\Os_Cfg.h

        - 04_HexDriver_2_Bin.zip
          Archive containing a file called hex2bin.exe that should have been used to convert the hex image generated by 03_Driver project above into a binary image.
          
        - 05_VB6_Encrypt_DriverBin.zip
          Archive containing the source files and Visual Basic 6.0 project of an application that was supposed to encrypt the bin file generated by 04_HexDriver_2_Bin.
          
        - 06_VB6_EncryptedDriver_Loader.zip
          Archive containing the source files and Visual Basic 6.0 project of an application that was supposed to run on the PC and communicate over UART with the SSB application running on Ao-to One. During communication, the encrypted image of the Ao-to One software created by 05_VB6_Encrypt_DriverBin at step above should have been decrypted by the SSB and written in the Flash of the Ao-to One board, becoming thus available to be run next time when Ao-to One board was starting, if the boot jumper was telling SSB it should run app from Flash and not wait on the Uart for a new image.
         
        - 07_VB4A_Dashboard.zip
          Archive containing the B4A (Basic for Android) project that will generate the Android application used to display information from and control the Ao-to One board. The project is the one that has been used when shooting the movies for the Kickstarter campaign.
          
        - 08_Aoto_One.zip
          Archive containing the source files and IAR project for Ao-to One application, as of 2nd of Nov 2015, just before movies for Kickstarter campaign were shot. This is the image for the Ao-to One board that was put in the car platform, the one that appears in these 2 movies:
            - https://youtu.be/jRzxxN6UutY
            - https://youtu.be/2NRxXM6jYkI
            
          The following files are empty and do not contain any code, due to the fact that their code was started from some sample code files released in source code by Freescale in it's 802.15.4 stacks (SynkroRF, RF4CE, Zigbee) codebases and the original header in these files state that no part of the code can be reproduced in any form without specific written permission from Freescale Semiconductor. For this reason, the IAR project will of course not compile. If approval from NXP to make these files available as open source will be obtained, their content will be added in the archive later on.
            - 08_Aoto_One\Source\Platform\Base\FunctionLib.c
            - 08_Aoto_One\Source\Platform\Base\FunctionLib.h
            - 08_Aoto_One\Source\Platform\Nvm\Nvm_Interface.h
            - 08_Aoto_One\Source\Platform\Nvm\NV_Flash.c
            - 08_Aoto_One\Source\Platform\Nvm\NV_Flash.h
            - 08_Aoto_One\Source\Platform\Nvm\NV_FlashHAL.c
            - 08_Aoto_One\Source\Platform\Nvm\NV_FlashHAL.h
            - 08_Aoto_One\Source\Platform\Tmr\Tmr.c
            - 08_Aoto_One\Source\Platform\Tmr\Tmr.h
            - 08_Aoto_One\Source\Platform\Tmr\Tmr_Config.h
            - 08_Aoto_One\Source\Platform\Uart\Uart.c
            - 08_Aoto_One\Source\Platform\Uart\Uart.h
            - 08_Aoto_One\Source\Ts\Ts.c
            - 08_Aoto_One\Source\Ts\Ts.h
            - 08_Aoto_One\Source\Ts\Ts_Cfg.h
